#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read1(self):
        s = "100 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  100)
        self.assertEqual(j, 10)

    def test_read2(self):
        s = "201 210\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  201)
        self.assertEqual(j, 210)

    def test_read3(self):
        s = "900 1000\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  900)
        self.assertEqual(j, 1000)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)


    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)


    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)


    def test_eval_5(self):
        v1 = collatz_eval(11, 40)
        self.assertEqual(v1, 112)

    def test_eval_6(self):
        v2 = collatz_eval(40, 11)
        self.assertEqual(v2, 112)

    def test_eval_7(self):
        v1 = collatz_eval(1000, 10000)
        self.assertEqual(v1, 262)

    def test_eval_8(self):
        v2 = collatz_eval(1, 1)
        self.assertEqual(v2, 1)

    def test_eval_9(self):
        v1 = collatz_eval(9000, 10000)
        self.assertEqual(v1, 260)


    def test_eval_10(self):
        v1 = collatz_eval(1000,10)
        self.assertEqual(v1, 179)

    def test_eval_11(self):
        v2 = collatz_eval(1, 100000)
        self.assertEqual(v2, 351)

    def test_eval_12(self):
        v2 = collatz_eval(100000,10000)
        self.assertEqual(v2, 351)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print1(self):
        w = StringIO()
        collatz_print(w, 1000, 10, 179)
        self.assertEqual(w.getvalue(), "1000 10 179\n")

    def test_print2(self):
        w = StringIO()
        collatz_print(w, 100000, 10000, 351)
        self.assertEqual(w.getvalue(), "100000 10000 351\n")

    def test_print3(self):
        w = StringIO()
        collatz_print(w, 1, 1, 1)
        self.assertEqual(w.getvalue(), "1 1 1\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve1(self):
        r = StringIO("100000 10000\n1 100000\n1000 10\n9000 10000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "100000 10000 351\n1 100000 351\n1000 10 179\n9000 10000 260\n")

    def test_solve2(self):
        r = StringIO("1 1\n1000 10000\n40 11\n11 40\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 1 1\n1000 10000 262\n40 11 112\n11 40 112\n")


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
